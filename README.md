# Donut Bob ! (Travail pratique 3)

## Description

Ce projet est un jeu vidéo de plateformes en C qui permet à un utilisateur de faire
évoluer un personnage dans un environnement en 2 dimensions. Le but du jeu est
d'utiliser un avatar pour ramasser des beignes distribués dans une carte de jeu.
Avant de commencer la partie, le joueur se voie présenté l'ordre dans laquelle les
beignes doivent être ramassé. Quand tous les beignes sont collecté,
l’utilisateur sera présenté avec un écran lui indiquant ses succès et échec et
un pointage qui tient compte des chaines de beignes correctement collecté.

Le projet est fait dans le cadre du cours INF3135 à l'UQAM, durant la session
d’automne 2017.

## Auteurs

- Eric Pietrocupo (PIEE14027701)
- Nicolas Lambert (LAMN19068801)

## Fonctionnement

But:
- Le joueur doit mémoriser une série de beigne et ensuite les collecter
  à l'aide d'un avatar dans un tableau en 2 dimensions.

Contrôle:
 - ESC - Quitte le jeu si nous somme dans la page d'accueil. Retourne à la page
   d'accueil dans les autres cas.
 - [↓] Permet de naviguer dans le menu principal.
 - [↑] Permet de naviguer dans le menu principal ET de faire des sauts avec Bob
 - [←] Permet de bouger Bob vers la gauche
 - [→] Permet de bouger Bob vers la Droite

## Plateformes supportées

Le programme est testé sur Ubuntu 17.1 et Debian 9

## Dépendances

* SDL2 (https://www.libsdl.org/download-2.0.php)
* SDL2_image (https://www.libsdl.org/projects/SDL_image/)
* SDL2_ttf (https://www.libsdl.org/projects/SDL_ttf/)

## Compilation

Simplement utiliser la commande ci-bas dans le répertoire src.

~~~
make
~~~

Pour Executer le programme, tapez:

~~~
./DonutBob
~~~

## Références

- maze de Alexandre Blondin Masse (https://bitbucket.org/ablondin-projects/maze-sdl/overview)
- Space Defender de Eric Pietrocupo (https://gitlab.com/ericp626/SpaceDefender)

## Division des tâches

Liste des tâches de chacun des membres de l'équipe.

- [X] Ajout du menu principale (Eric)
- [X] Conception des plateformes (Nicolas)
- [X] Ajout de la de détection de colision avec la carte (Eric)
- [X] Ajouter les images beignes dans la carte (Nicolas)
- [X] Animer les beignes (Eric)
- [X] Ajout du sprite de Bob avec le mouvement de Droit et Gauche (Eric)
- [X] Ajout du saut et de l’effet de gravité (Eric)
- [X] Ajout de l’interaction entre Bob et les beignes + la victoire du joueur (Nicolas)
- [X] Victoire du joueur (Nicolas)
- [X] Ajout du systeme de pointage (Eric)
- [X] Creation des nouvelles images (Eric)
- [X] Chargement du niveau a partir d'un fichier et affichage de celui-ci (Nicolas)

## Statut

Projet Complété sans bug. Ajout de fonctionnalités supplémentaires optionnels.
