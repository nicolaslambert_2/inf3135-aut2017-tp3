CC = gcc
OBJS = $(patsubst src/%.c,obj/%.o,$(wildcard src/*.c))
CFLAGS = `sdl2-config --cflags` -O3 -std=c11
LDFLAGS = -lSDL2_image -lSDL2_ttf `sdl2-config --libs`
EXEC = DonutBob

Release: obj $(EXEC)

$(EXEC): $(OBJS)
	$(CC) -o $(EXEC) $(OBJS) $(LDFLAGS) 

obj/%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) -o $@ -c $<

obj:
	mkdir obj


.PHONY: clean Release cleanRelease

cleanRelease: clean

clean:
	rm -rf obj
	rm -f $(EXEC)

