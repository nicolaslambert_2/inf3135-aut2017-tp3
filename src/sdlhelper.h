/**
 * A module that include method that are missing from SDL, making SDL more
 * convenient to use.
 */

#ifndef SDLHELPER_H_INCLUDED
#define SDLHELPER_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

/**
 * Create a texture containing a text message using a message and a font.
 * Once the texture is created, it's in video memory and can be printed multiple
 * times, but it cannot be modified. Use SDL_DestroyTexture to free the
 * memory once finished.
 *
 * @param message    String message to create
 * @param font       SDL font to be used
 * @param color      Main color of the font
 * @param mode       Rendering method that will be used
 * @param ren        Renderer to create the texture
 * @return a texture containing the string of text.
 */

SDL_Texture *textmessage_create ( char *message, TTF_Font *font, SDL_Color color,
                                    eRenderMode mode, SDL_Renderer *ren );

/**
 * Render a texture on the screen using a target x, y position. This method
 * calls SDL_QueryTexture everytime, use if the texture you are redering is not
 * always the same size. I will also copy the source entirely to the destination
 *
 * @param tex  Texture to render on the screen
 * @param x    x target position
 * @param y    y target position
 * @param ren  Renderer used to draw the texture
 */

void texture_render ( SDL_Texture *tex, int x, int y, SDL_Renderer *ren );

#endif // SDLHELPER_H_INCLUDED
