#ifndef SPRITESHEET_H
#define SPRITESHEET_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

struct Spritesheet {
    int numRows;            // The number of rows in the sprite sheet
    int numColumns;         // The number of columns in the sprite sheet
    int spriteWidth;        // The width of an individual sprite
    int spriteHeight;       // The height of an individual sprite
    SDL_Texture *texture;   // The texture (image) of the sprite sheet
    SDL_Renderer *renderer; // The renderer
};

/**
 * Creates a sprite sheet.
 *
 * @param filename    The filename of the sprite sheet image
 * @param numRows     The number of rows in the sprite sheet
 * @param numColumns  The number of columns in the sprite sheet
 * @param numSprites  The total number of sprites
 * @param renderer    The renderer used for loading the sprite sheet
 * @return            The sprite sheet
 */
struct Spritesheet *Spritesheet_create(const char *filename,
                                       int numRows, int numColumns,
                                       SDL_Renderer* renderer);

/**
 * Delete the sprite sheet.
 *
 * @param spritesheet  The spritesheet to delete
 */
void Spritesheet_delete(struct Spritesheet *spritesheet);

/**
 * Renders the current sprite.
 *
 * NOTE: Frames are identified as x + ( y * nbColumns )
  *
 * @param spritesheet  The spritesheet to render
 * @param x            The top-left corner x-coordinate for the render
 * @param y            The top-left corner y-coordinate for the render
 * @param sprite       The sprite to render
 *
 */

void Spritesheet_render(struct Spritesheet *spritesheet,
                        int x, int y, int frame);

/**
 * Renders the current sprite with a specific target size. Ignore scaling
 * factor.
 *
 * @param spritesheet  The spritesheet to render
 * @param x            The top-left corner x-coordinate for the render
 * @param y            The top-left corner y-coordinate for the render
 * @param sprite       The frame number ot render
 * @param w            Target width of the sprite
 * @param h            Target Height of the sprite
 */

void Spritesheet_render_size (struct Spritesheet *spritesheet,
                        int x, int y, int frame, int w, int h);


#endif
