#include <stdio.h>
#include "font.h"
#include "system.h"
#include "map.h"
#include "spritesheet.h"
#include "animated_spritesheet.h"
#include "donut.h"

void drawBackground ( struct Spritesheet *sprite, SDL_Renderer *ren, int frameID1,
                        int frameID2, SDL_Color bgcolor, int nbTile )
{
   int x;
   int y;
   int size = ( SCREEN_WIDTH / nbTile );
   int nb_x = nbTile;
   int nb_y = ( SCREEN_HEIGHT / size ) + 1;
   int xpos = 0; // used to speed up the process
   int ypos = 0; // used to speed up the process
   int frame;

   SDL_SetRenderDrawColor(ren,
       bgcolor.r, bgcolor.g, bgcolor.b, bgcolor.a );
   SDL_RenderClear( ren );


   for ( y = 0 ; y < nb_y ; y++)
   {
      for ( x = 0 ; x < nb_x ; x++)
      {
         frame = frameID2;
         if ( y % 2 == 0 && x % 2 == 0) frame = frameID1;
         else if ( y % 2 == 1 && x % 2 == 1) frame = frameID1;

         Spritesheet_render_size( sprite, xpos, ypos, frame, size, size );
         xpos += size;
      }
      ypos += size;
      xpos = 0;
   }

   //reset the default color
   SDL_SetRenderDrawColor(ren, 0, 0, 0, 255 );

}


struct Map *loadMap(char *name, const sApplication *app, const sResources *res)
{
  int y,x;
  FILE * fp;

  struct Map *map;
  map = (struct Map*)malloc(sizeof(struct Map));


  fp = fopen(name, "r");

  if (fp == NULL)
  {
    printf("Error reading the map");
    exit(1);
  }
  //Loading the values of the file into the Array
  for (y = 0; y < MAX_MAP_Y; y++)
  {
    for (x = 0; x < MAX_MAP_X; x++)
    {

      int count = fscanf(fp, "%d", &map->tile[y][x]); //int count to catch the return value of fscan and stop the compiler warning.

    }
  }

  /* Close the file afterwards */
  fclose(fp);
  return map;
}

void drawMap(const sApplication *app, const sResources *res)
{

  //size and position of current tile
  SDL_Rect tile_pos;
  tile_pos.w = MAP_BLOCK_SIZE;
  tile_pos.h = MAP_BLOCK_SIZE;
  tile_pos.x = 0;
  tile_pos.y = 0;

  int y,x;

  struct Map *map = res->map;

  for (y = 0; y < MAX_MAP_Y; y++)
  {
    for (x = 0; x < MAX_MAP_X; x++)
    {
      if (map->tile[y][x] == 1)
      {
         Spritesheet_render_size( res->spr_stage, tile_pos.x, tile_pos.y, 3,
                                            MAP_BLOCK_SIZE, MAP_BLOCK_SIZE );
      }
      tile_pos.x += MAP_BLOCK_SIZE;
    }
    tile_pos.x = 0;
    tile_pos.y += MAP_BLOCK_SIZE;

  }
  tile_pos.y = 0;
}

void Map_delete(struct Map *map) {
    if (map != NULL) {
        free(map);
    }
}
