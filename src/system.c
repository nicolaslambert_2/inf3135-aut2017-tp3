/**
 * System specific methods
 */

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <time.h>
#include "font.h"
#include "system.h"
#include "spritesheet.h"
#include "animated_spritesheet.h"
#include "donut.h"
#include "map.h"
#include "bob.h"
#include "map.h"
#include "score.h"

void clear_resource_pointers ( sResources *res )
{
   res->tex_title = NULL;
   res->fnt_donut64 = NULL;
   res->fnt_dunkin64 = NULL;
   res->fnt_dunkin32 = NULL;
   res->spr_bob = NULL;
   res->spr_donut = NULL;
   res->spr_stage = NULL;
   res->spr_score = NULL;
   res->bmf_dunkin64pink = NULL;
   res->map = NULL;
}

// --------------
// Public Methods
// --------------

bool initialise_SDL()
{
   printf("Initializing SDL\n");
   if ( SDL_Init (SDL_INIT_VIDEO) != 0)
   {
      printf ("SDL_Init Error: %s\n", SDL_GetError() );
      return false;
   }

   if ( IMG_Init ( IMG_INIT_PNG ) == 0)
   {
      printf ( "IMG_Init Error: loading the PNG image processing\n");
      return false;
   }

   if ( TTF_Init () != 0 )
   {
      printf ( "TTF_Init Error: Cannot load the TTF module\n");
      return false;
   }

   // Intializes random number generator
   time_t t;
   srand((unsigned) time(&t));

   return true;
}

void close_SDL ()
{
   printf("Closing SDL\n");
   TTF_Quit();
   IMG_Quit();
   SDL_Quit();
}

bool load_resources ( sResources *res, const sApplication *app )
{
   printf("Loading Resources\n");
   // temporary surfaces to build textures

   SDL_Surface *bmp_title;
   SDL_Surface *png_background;
   SDL_Surface *png_groundImage1;

   SDL_Color fnt_color = { .a= 255, .r=255, .g=150, .b=200 };

   clear_resource_pointers( res );

   bmp_title = IMG_Load("assets/title.png");
   if ( bmp_title == NULL )
   {
      printf ("Error: Cannot load Title Bitmap\n");
      return false;
   }

   res->tex_title = SDL_CreateTextureFromSurface (app->ren, bmp_title);
   SDL_FreeSurface( bmp_title );
   if ( res->tex_title == NULL )
   {
      printf ("Error: Cannot create Title texture\n");
      return false;
   }

   res->fnt_donut64 = TTF_OpenFont("assets/pwyummydonuts.ttf", 64);
   if ( res->fnt_donut64 == NULL)
   {
      printf ( "Error: Cannot load True Type Font: Donuts 64\n");
      return false;
   }

   res->fnt_dunkin64 = TTF_OpenFont("assets/dunkin.ttf", 64);
   if ( res->fnt_dunkin64 == NULL)
   {
      printf ( "Error: Cannot load True Type Font: Dunkin 64\n");
      return false;
   }

   res->fnt_dunkin32 = TTF_OpenFont("assets/dunkin.ttf", 32);
   if ( res->fnt_dunkin32 == NULL)
   {
      printf ( "Error: Cannot load True Type Font: Dunkin 32\n");
      return false;
   }

   res->bmf_dunkin64pink = bitmapfont_create( res->fnt_dunkin64, BLENDED, fnt_color, app->ren );
   if ( res->bmf_dunkin64pink == NULL)
   {
      printf ( "Error: Cannot Create bitmapfont : Dunkin 64 pink\n");
      return false;
   }

   res->spr_donut = Spritesheet_create( DONUT_FILENAME,
      DONUT_NB_ROWS, DONUT_NB_COLS, app->ren );
   if ( res->spr_donut == NULL )
   {
      printf ("Error: Cannot create Donut Sprite Sheet\n");
      return false;
   }

   res->spr_bob = Spritesheet_create( BOB_FILENAME, BOB_NB_ROWS, BOB_NB_COLS,  app->ren );
   if ( res->spr_bob == NULL )
   {
      printf ("Error: Cannot create Bob's Sprite Sheet\n");
      return false;
   }

   res->spr_stage = Spritesheet_create( MAP_TEXTURE_FILENAME, MAP_TEXTURE_HEIGHT,
                     MAP_TEXTURE_HEIGHT, app->ren );
   if ( res->spr_stage == NULL )
   {
      printf ("Error: Cannot load stage textures\n");
      return false;
   }

   res->spr_score = Spritesheet_create( SCORE_TEXTURE_FILENAME, SCORE_TEX_NBROWS,
                     SCORE_TEX_NBCOLS, app->ren );
   if ( res->spr_score == NULL )
   {
      printf ("Error: Cannot load scoring textures\n");
      return false;
   }

   //Call the function to load all the map assets and save them in the game
   //ressources
   res->map = loadMap("assets/level.txt", app, res);
   if ( res->map == NULL )
   {
      printf ("Error: Cannot load the map\n");
      return false;
   }

   return true;
}

void unload_resources ( sResources *res )
{
   printf("Clearing Resources\n");
   if ( res->tex_title != NULL )    SDL_DestroyTexture(res->tex_title);
   if ( res->fnt_donut64 != NULL )    TTF_CloseFont ( res->fnt_donut64 );
   if ( res->fnt_dunkin64 != NULL )    TTF_CloseFont ( res->fnt_dunkin64 );
   if ( res->fnt_dunkin32 != NULL )    TTF_CloseFont ( res->fnt_dunkin32 );
   if ( res->spr_donut != NULL )    Spritesheet_delete(res->spr_donut);
   if ( res->spr_bob != NULL )    Spritesheet_delete(res->spr_bob);
   if ( res->map != NULL )    Map_delete(res->map);
   if ( res->spr_stage != NULL )    Spritesheet_delete(res->spr_stage);
   if ( res->spr_score != NULL )    Spritesheet_delete(res->spr_score);
   bitmapfont_destroy( res->bmf_dunkin64pink );
   clear_resource_pointers( res );
}

bool create_application ( sApplication *app )
{
   printf("Creating Application\n");
   app->win = NULL;
   app->ren = NULL;

   app->win = SDL_CreateWindow ("Donut Bob", 300, 100,
         SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

   if ( app->win == NULL )
   {
      printf ("Error: Cannot create window\n");
      return false;
   }

   app->ren = SDL_CreateRenderer
            (app->win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
   if ( app->ren == NULL )
   {
      printf ("Error: Cannot create renderer\n");
      return false;
   }

   return true;
}


void destroy_application ( sApplication *app )
{
   printf("Destroying Application\n");
   if ( app->ren != NULL ) SDL_DestroyRenderer(app->ren);
   if ( app->win != NULL ) SDL_DestroyWindow(app->win);

   app->win = NULL;
   app->ren = NULL;
}
