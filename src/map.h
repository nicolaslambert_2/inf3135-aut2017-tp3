/**
 * Map specific methods
 */

#ifndef MAP_H_INCLUDED
#define MAP_H_INCLUDED

#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

//number of tiles
#define MAX_MAP_Y 15
#define MAX_MAP_X 20

#define MAP_BLOCK_SIZE 32
#define MAP_SOLID      1
#define MAP_DONUT      2

#define MAX_STAGE_DONUTS 9

struct Map
{
  int tile[MAX_MAP_Y][MAX_MAP_X];
};

/**
 * Clear the screen and Draw the whole background using the cookie tiles.
 * Can be used for other purpose than stage rendering. The rendering is
 * use a checkered patern, use the same frame id to get a uniform background.
 * The method will use the width of the screen to fit as many tile as
 * requested by nbTile, the height will simply be filled up to screen height.
 *
 * @param res        Structure of resources
 * @param ren        Renderer
 * @param frameID1   1st id of the sprite frame to draw
 * @param frameID2   2nd id of the sprite frame to draw
 * @param bgcolor    Background color to initialise the screen
 * @param nbTile    Nb of tile along the width of the screen
 */
void drawBackground ( struct Spritesheet *sprite, SDL_Renderer *ren, int frameID1,
                        int frameID2, SDL_Color bgcolor, int nbTile );


/**
* Add the level from a txt file to a 2d array. Each values will be use to renderer
* the correct texture. This will be loaded in the Map struct.
*
* @param name    The filename of the level in txt
* @param app reference to the application
* @param res reference to the resources used by all screens
* @return a Map structure containing the tiles of the map.
*/
struct Map *loadMap(char *name, const sApplication *app, const sResources *res);

/**
* Create the render copy of the map to present later.
* Does not clear the background.
*
* @param app reference to the application
* @param res reference to the resources used by all screens
*/
void drawMap(const sApplication *app, const sResources *res);

/**
 * Deletes the Map.
 *
 * @param Map  The map of the level
 */
void Map_delete(struct Map *map);

#endif // MAP_H_INCLUDED
