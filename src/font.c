

#include "font.h"
#include "system.h"
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_pixels.h>



//---------------------------
// Private Methods
//---------------------------


//---------------------------
// Public methods
//---------------------------

sBitmapFont *bitmapfont_create ( TTF_Font *font, eRenderMode mode,
                                 SDL_Color color, SDL_Renderer *ren )
{
   sBitmapFont *bmf = (sBitmapFont*) malloc ( sizeof (sBitmapFont));
   SDL_Surface *glyph[NB_GLYPH];
   int i = 0;
   SDL_Rect srcrect;
   int total_width = 0;
   int highest_height = 0;
   SDL_Surface *tmpsurf = NULL;
   int x_offset = 0;
   SDL_Color shade_color = { .a=255, .r=0, .g=0, .b=0 };

   //calculate the surface size and load the glyphs
   for ( i = 0; i < NB_GLYPH ; i++)
   {
      switch ( mode )
      {
         default:
         case SOLID:
            glyph [i]= TTF_RenderGlyph_Solid( font, '0' + i, color );
         break;
         case SHADED:
            glyph [i]= TTF_RenderGlyph_Shaded( font, '0' + i, color, shade_color );
         break;
         case BLENDED:
            glyph [i]= TTF_RenderGlyph_Blended( font, '0' + i, color );
         break;
      }

      SDL_GetClipRect( glyph[i], &bmf->rect[i]);

      if ( bmf->rect[i].h > highest_height)
      {
         highest_height = bmf->rect[i].h;
      }
      total_width += bmf->rect[i].w;
   }

   //   tmpsurf = SDL_CreateRGBSurfaceWithFormat(0, total_width, highest_height, 32,
   //                                             SDL_PIXELFORMAT_RGBA32);

   // NOTE: Changed to avoid a pipeline bug due to a difference in SDL version
   // which makes SDL_CreateRGBSurfaceWithFormat and SDL_PIXELFORMAT_RGBA32 unavailable
   // Code copy pasted from the documentation

   Uint32 rmask, gmask, bmask, amask;

    /* SDL interprets each pixel as a 32-bit number, so our masks must depend
       on the endianness (byte order) of the machine */
   #if SDL_BYTEORDER == SDL_BIG_ENDIAN
      rmask = 0xff000000;
      gmask = 0x00ff0000;
      bmask = 0x0000ff00;
      amask = 0x000000ff;
   #else
      rmask = 0x000000ff;
      gmask = 0x0000ff00;
      bmask = 0x00ff0000;
      amask = 0xff000000;
   #endif

   tmpsurf = SDL_CreateRGBSurface( 0, total_width, highest_height, 32,
                                    rmask, gmask, bmask, amask);

   //Blit and free the glyphs
   for ( i = 0 ; i < NB_GLYPH; i++ )
   {
      srcrect = bmf->rect[i];
      bmf->rect[i].x = x_offset;

      SDL_BlitSurface ( glyph [i], &srcrect, tmpsurf, &bmf->rect[i]);

      x_offset += srcrect.w;
      SDL_FreeSurface( glyph [i]);
   }

   bmf->tex = SDL_CreateTextureFromSurface( ren, tmpsurf );
   SDL_FreeSurface( tmpsurf );

   return bmf;
}

void bitmapfont_render ( const sBitmapFont *bmpfont, unsigned int value, int x, int y
                                                               , SDL_Renderer *ren  )
{
   char str [ INTEGER_NB_CHAR ] = "\0";
   int i = 0;
   int x_offset = x;
   SDL_Rect dst_rect;
   int index;

   sprintf ( str, "%u", value );

   while ( str[i] != '\0')
   {
      index = str[i] - '0'; //get the value of the character

      //NOTE: we assume that all character will be between '0' and '9' to optimise speed.
      dst_rect = bmpfont->rect[index];
      dst_rect.x = x_offset;
      dst_rect.y = y;
      SDL_RenderCopy ( ren, bmpfont->tex, &bmpfont->rect[index], &dst_rect );
      x_offset += bmpfont->rect[index].w;

      i++;
   }

}

void bitmapfont_destroy ( sBitmapFont *bmpfont )
{
   if ( bmpfont != NULL )
   {
      SDL_DestroyTexture( bmpfont->tex);
      free ( bmpfont );
   }
}

int bitmapfont_get_width ( sBitmapFont *bmpfont )
{
   int largest_width = 0;
   int i;

   for ( i = 0 ; i < NB_GLYPH ; i++)
   {
      if ( bmpfont->rect[i] .w > largest_width)
      {
         largest_width = bmpfont->rect[i] .w;
      }
   }

   return largest_width;
}

