#include "spritesheet.h"
#include "font.h"
#include "system.h"
#include "test.h"
#include "sdlhelper.h"
#include "donut.h"
#include "map.h"
#include "bob.h"
#include "score.h"



void test_bitmapfont ( const sApplication *app, const sResources *res )
{
   SDL_Color fnt_color = { .a= 255, .r=255, .g=150, .b=200 };
   SDL_Rect tmp_rect = { .x=100, .y =100, .h=0, .w=0 };

   //sBitmapFont *bmf = bitmapfont_create( res->fnt_dunkin64, BLENDED, fnt_color, app->ren);
   SDL_Texture *msg1 = textmessage_create("Allo", res->fnt_donut64 , fnt_color, BLENDED, app->ren);
   SDL_Texture *msg2 = textmessage_create("Bonjour", res->fnt_donut64 , fnt_color, BLENDED, app->ren);

   SDL_RenderClear ( app->ren );
   SDL_QueryTexture( res->bmf_dunkin64pink->tex, NULL, NULL, &tmp_rect.w, &tmp_rect.h );

   SDL_RenderCopy ( app->ren, res->bmf_dunkin64pink->tex, NULL, &tmp_rect );

   SDL_RenderPresent ( app->ren );
   SDL_Delay ( 1000 );

   //-----------------------------------

   //SDL_Rect msgpos;

   int starticks = SDL_GetTicks();
   int endticks = starticks + 5000;
   int currenticks = starticks;

   while ( currenticks < endticks)
   {
      SDL_RenderClear ( app->ren );
      bitmapfont_render( res->bmf_dunkin64pink, currenticks - starticks, 100, 100, app->ren );

      texture_render( msg1, 100, 200, app->ren );
      texture_render( msg2, 100, 300, app->ren );

      SDL_RenderPresent ( app->ren );
      currenticks = SDL_GetTicks();
   }



}

void test_bob_movement ( const sApplication *app, const sResources *res )
{
   printf ("DEBUG: Testing Bob Movement\n");
   SDL_Event evn;
   bool quit = false;
   SDL_Color bgcolor = {.r=0, .g=0, .b=0, .a=255 };
   sBobSprite *bob = bob_create( res, MAP_BLOCK_SIZE * 2 );

   while ( quit == false )
   {
      // --- read input ----
      while (SDL_PollEvent(&evn) == 1)
      {
         if (evn.type == SDL_QUIT) quit = true;

         if (evn.type == SDL_KEYDOWN )
         {
            if ( evn.key.keysym.scancode == SDL_SCANCODE_ESCAPE )
            {
               quit = true;
            }
         }
         bob_give_input( bob, &evn );
		}

		// --- Update and render ---

		SDL_RenderClear ( app->ren );

		drawBackground( res->spr_stage, app->ren, 0, 0, bgcolor, 10 );
		bob_render ( bob , res->map);

		SDL_RenderPresent ( app->ren );
   }


   bob_delete( bob );
}

void test_draw_background ( const sApplication *app, const sResources *res )
{
   printf ("DEBUG: Testing Draw Background \n");
   bool quit = false;
   SDL_Color bgcolor = {.r=231, .g=156, .b=66, .a=255 };
   SDL_Event evn;

   while ( quit == false)
   {
      while (SDL_PollEvent(&evn) == 1)
      {
         if (evn.type == SDL_QUIT) quit = true;

         if ( evn.key.keysym.scancode == SDL_SCANCODE_ESCAPE )
         {
            quit = true;
         }
      }

      drawBackground( res->spr_stage, app->ren, 1, 2, bgcolor, 8 );

      SDL_RenderPresent ( app->ren );
   }
}

void test_score_track ( const sApplication *app, const sResources *res )
{
   int i, j;
   int nb_donut = 9;
   sScoreTrack *track = scoretrack_create( nb_donut );


   // collect donut in numerical order
   for ( i = 0 ; i < nb_donut ; i++ )
   {
      scoretrack_add( track, i );
      //printf ("\nLast collected is %d\n", scoretrack_last( track ));
      //if ( scoretrack_iscomplete ( track) == true ) printf("Colletion Complete\n");
      //show donuts
      //printf ("\nCollected are: ");


   }

   printf ("\n");
   for ( j = 0 ; j < nb_donut ; j++) printf ("[%d]", track->collect_sequence [ j ]);

   printf ("\n");

   for ( i = 0 ; i < nb_donut - 1 ; i++)
   {
      printf ("[%d]", track->collect_sequence [ i ]);
      if ( scoretrack_get_chain ( track, i ) == true )
      {  printf ("---");
      }
      else
      {  printf (" X ");
      }
   }
   printf ("[%d]\n", track->collect_sequence [ nb_donut - 1 ]); // the last one

   printf ("DEBUG: Final Score %d", scoretrack_get_score ( track ) );
   scoretrack_delete( track );
}
