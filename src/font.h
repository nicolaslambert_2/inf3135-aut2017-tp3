/**
* A module that allows the creation of bitmap fonts because SDL does not supply any.
* The only restriction is that it can only print unsigned integers
*
* There is also a few helper methods to make text message generation and less a pain.
*/

#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>


#define NB_GLYPH        10
#define INTEGER_NB_CHAR 11 // Size to hold integer value up to 4294967295 as a string.

typedef struct
{
   SDL_Texture *tex;
   SDL_Rect rect [ NB_GLYPH ]; //Position of the glyph in the texture, glyphs are 0 to 9
}
sBitmapFont;

// Type to change which method is used for creating the textures. Not affected
// in realtime because text strings and bitmap fonts should be generated
// outside the main loop.
typedef enum
{
   SOLID,
   SHADED,
   BLENDED
}
eRenderMode;

/**
 * Generate and save a bitmap font that includes only the number glyphs.
 * Do not generate in a real time loop. Create the font first then use
 * bitmapfont render to use it in a real time loop.
 *
 * @param font       SDL font to be used for the rendering
 * @param mode       Function that will be used to render the glyphs
 *                   SHADED mode will use black as background
 * @param color      Color of the font
 * @param ren        Renderer to create the textures
 * @return a pointer to a bitmapfont structure containing the created font.
 */
sBitmapFont *bitmapfont_create ( TTF_Font *bmpfont, eRenderMode mode,
                                 SDL_Color color, SDL_Renderer *ren);
/**
 * Converts an integer into text and print on the screen the coresponding
 * digits to display that number. Designed to be called in realtime with
 * changing values.
 *
 * NOTE: I use sprintf to do the conversion.
 *
 * @param bmpfont font to be used for drawing the numbers.
 * @param value      to display on the screen
 * @param x          position to start drawing numbers
 * @param y          position to start drawing numbers
 * @param ren        Renderer to blit the textures
 */
void bitmapfont_render ( const sBitmapFont *bmpfont, unsigned int value, int x, int y,
                                                                   SDL_Renderer *ren  );

/**
 * Destroy a bitmap font if the pointer is non-null
 *
 * @param bmpfont    Pointer on a sBitmapFont structure to be free
 */
void bitmapfont_destroy ( sBitmapFont *bmpfont );

/**
 * Get the size of the widest character
 *
 * @param bmpfont    Pointer on a sBitmapFont
 * @return the width of the widest glyph
 */

int bitmapfont_get_width ( sBitmapFont *bmpfont );



#endif // FONT_H_INCLUDED
