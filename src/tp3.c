/**
 * tp3.c - Initializes SDL
 */

#include <stdio.h>
#include <SDL2/SDL.h>
#include "font.h"
#include "system.h"
#include "map.h"
#include "score.h"
#include "screen.h"

int main(void)
{

   sApplication app;
   sResources res;
   eScreen active_screen = TITLE_SCREEN;
   bool quit = false;

   if ( initialise_SDL() == true )
   {
      if ( create_application(&app) == true )
      {
         if ( load_resources(&res, &app) == true)
         {
            printf ("Starting the game\n");
            printf ("------------------------------------------------\n");
            sScoreTrack *score = scoretrack_create ( MAX_STAGE_DONUTS );
            while ( quit == false )
            {
               switch (active_screen)
               {
                  case TITLE_SCREEN:
                     active_screen = title_screen_mainloop( &app, &res );
                  break;
                  case GAME_SCREEN:
                     active_screen = game_screen_mainloop( &app, &res, score );
                  break;
                  case SCORE_SCREEN:
                     active_screen = score_screen_mainloop( &app, &res, score );
                  break;
                  case READY_SCREEN:
                     scoretrack_reset ( score );
                     active_screen = ready_screen_mainloop( &app, &res, score );
                  break;
                  case EXIT_GAME:
                     quit = true;
                  break;
                  case TEST_SCREEN:
                     active_screen = test_screen_mainloop( &app, &res );
                  break;
                  default:
                     printf("ERROR: I refuse to switch screen, active_screen = %d\n", active_screen);
                  break;
               }
               //NOTE: this switch is not run in realtime, so it's safe
            }
            scoretrack_delete( score );
            printf ("------------------------------------------------\n");
         }
         unload_resources( &res);
      }
      destroy_application(&app);
      close_SDL();
   }

}
