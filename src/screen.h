/**
* Handle the main loops of the various screens of the game
*/

#ifndef SCREEN_H_INCLUDED
#define SCREEN_H_INCLUDED



#define MENU_CURSOR_SPEED  128
#define NB_MENU            2

//identification of screens to switch the the right method
//
//An alternavive is to use function pointers, but since the number of screens
//is limited, the flow between screen is pretty fixed and there is no screen
//stacks, let's keep it simple.

typedef enum
{
   EXIT_GAME,
   TITLE_SCREEN,
   GAME_SCREEN,
   SCORE_SCREEN,
   READY_SCREEN,
   TEST_SCREEN
}eScreen;

/**
* Main loop to handle the title screen in realtime
* @param app reference to the application
* @param res reference to the resources used by all screens
* @return the value to the next screen to switch to done in the main
*/

eScreen title_screen_mainloop ( const sApplication *app, const sResources *res );

/**
* Main loop to handle the ready screen before the game screen
* @param app reference to the application
* @param res reference to the resources used by all screens
* @return the value to the next screen to switch to done in the main
*/

eScreen ready_screen_mainloop ( const sApplication *app, const sResources *res,
                                sScoreTrack *score );


/**
* Main loop to handle the game screen in realtime
* @param app reference to the application
* @param res reference to the resources used by all screens
* @return the value to the next screen to switch to done in the main
*/

eScreen game_screen_mainloop ( const sApplication *app, const sResources *res,
                               sScoreTrack *score );

/**
* Main loop to handle the scoring screen on success or failure
* @param app reference to the application
* @param res reference to the resources used by all screens
* @return the value to the next screen to switch to done in the main
*/

eScreen score_screen_mainloop ( const sApplication *app, const sResources *res,
                                sScoreTrack *score );

/**
* Main loop to handle the test screen in realtime
* a special screen for developing screens and testing features.
* @param app reference to the application
* @param res reference to the resources used by all screens
* @return the value to the next screen to switch to done in the main
*/
eScreen test_screen_mainloop ( const sApplication *app, const sResources *res );



#endif // SCREEN_H_INCLUDED
