#include "font.h"
#include "sdlhelper.h"

SDL_Texture *textmessage_create ( char *message, TTF_Font *font, SDL_Color color,
                                    eRenderMode mode, SDL_Renderer *ren )
{
   SDL_Surface *bmp;
   SDL_Color shade_color = { .a=255, .r=0, .g=0, .b=0 };
   SDL_Texture *tex;

   switch ( mode )
   {
      default:
      case SOLID:
         bmp = TTF_RenderText_Solid( font, message, color );
      break;
      case SHADED:
         bmp = TTF_RenderText_Shaded( font, message, color, shade_color );
      break;
      case BLENDED:
         bmp = TTF_RenderText_Blended( font, message, color );
      break;
   }

   tex = SDL_CreateTextureFromSurface ( ren, bmp );
   SDL_FreeSurface ( bmp );

   return tex;
}


void texture_render ( SDL_Texture *tex, int x, int y, SDL_Renderer *ren )
{
   SDL_Rect pos;

   SDL_QueryTexture( tex, NULL, NULL, &pos.w, &pos.h );
   pos.x = x;
   pos.y = y;
   SDL_RenderCopy ( ren, tex, NULL, &pos );

}
