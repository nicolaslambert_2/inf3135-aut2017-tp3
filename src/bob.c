#include "font.h"
#include "system.h"
#include "spritesheet.h"
#include "animated_spritesheet.h"
#include "donut.h"
#include "map.h"
#include "bob.h"
#include "score.h"
#include "screen.h"
#include <SDL2/SDL.h>

//----------------------
// Private Methods
//----------------------

/**
 * This method will check if the corresponding block in the stage is solid
 * but it will also check all blocks in between the line are solid or not. Used
 * to check one collision side at a time.
 *
 * NOTE: this mean that the supplied coordinates must be octogonal, diagonals
 * will just give invalid results.
 *       Also x1 and y1 must start lower or equal to x2 and y2.
 *
 * @param map   Map object to verify
 * @param x1    x coordonate of the first point in the map
 * @param y1    y coordonate of the first point in the map
 * @param x2    x coordonate of the second point in the map
 * @param y2    y coordonate of the second point in the map
 * @return true if there is a collision, else false.
*/

bool check_stage_collision ( struct Map *map, int x1, int y1, int x2, int y2 )
{
   bool collide = false;

   while ( collide == false && x1 <= x2 && y1 <= y2 )
   {
      if ( y1 >= 0 && y1 < MAX_MAP_Y && x1 >= 0 && x1 < MAX_MAP_X )
         { collide = ( map->tile [ y1 ] [ x1 ] == MAP_SOLID ); }

      // increment the opposite axis
      if ( x1 == x2 ) y1++;
      else x1++;
   }

   return collide;
}

/**
 * method that checks if there is a collision with the stage.
 *
 * NOTE: Considering the sprite of bob is pretty thin, the bottom collision
 * will be made like any of the sides since there is little change of
 * having a super toe bug (Character standing on a ledge with one toe)
 *
 * Also, the collision check is made 1 pixel forwar in prevision of a collision
 *
 * @param bob    The sprite to collide with the stage
 * @param map    The map to collide the stage with
 *
 * @return A list of collision boolean for all sides of the sprite.
 */
bool bob_collide_with_stage ( sBobSprite *bob, struct Map *map, eCollisionSide side)
{
   bool collide = false;
   int mapx;
   int mapy;
   int mapx2;
   int mapy2;
   int tmpx;
   int tmpy;

   if ( side == SIDE_UP )
   {
      tmpy = bob->bounding_box.y - 1;
      mapx = bob->bounding_box.x / MAP_BLOCK_SIZE;
      mapy = tmpy / MAP_BLOCK_SIZE;
      mapx2 = (bob->bounding_box.x + bob->bounding_box.w )/ MAP_BLOCK_SIZE;
      mapy2 = mapy;
      collide = check_stage_collision( map, mapx, mapy, mapx2, mapy2 );
   }
   else if ( side == SIDE_DOWN )
   {
      tmpy = bob->bounding_box.y + 1;
      mapx = bob->bounding_box.x / MAP_BLOCK_SIZE;
      mapy = ( tmpy + bob->bounding_box.h) / MAP_BLOCK_SIZE;
      mapx2 = ( bob->bounding_box.x + bob->bounding_box.w ) / MAP_BLOCK_SIZE;
      mapy2 = mapy;
      collide = check_stage_collision( map, mapx, mapy, mapx2, mapy2 );
   }
   else if ( side == SIDE_LEFT )
   {
      tmpx = bob->bounding_box.x - 1;
      mapx = tmpx / MAP_BLOCK_SIZE;
      mapy = bob->bounding_box.y / MAP_BLOCK_SIZE;
      mapx2 = mapx;
      mapy2 = (bob->bounding_box.y + bob->bounding_box.h )/ MAP_BLOCK_SIZE;
      collide = check_stage_collision( map, mapx, mapy, mapx2, mapy2 );
   }
   else if ( side == SIDE_RIGHT )
   {
      tmpx = bob->bounding_box.x + 1;
      mapx = ( tmpx + bob->bounding_box.w ) / MAP_BLOCK_SIZE;
      mapy = bob->bounding_box.y / MAP_BLOCK_SIZE;
      mapx2 = mapx;
      mapy2 = (bob->bounding_box.y + bob->bounding_box.h )/ MAP_BLOCK_SIZE;
      collide = check_stage_collision( map, mapx, mapy, mapx2, mapy2 );
   }
   //NOTE: still not using switch to gain speed

   return collide;
}

/**
 * Change animation sequence according to the current status and the new status
 * will also change the status of the direction or jump what ever if an animation
 * has been modified of not.
 *
 * NOTE: if value 0, also identified by NO_MOVE and NO_JUMP is passed in parameter
 * that means no modification is required to the move or jump status.
 *
 * @param bob     The sprite to modify
 * @param newdir  the new direction bob should move to
 * @param newjump The new jumping status
*/

void change_animation_sequence ( sBobSprite *bob, eMoveDirection newdir, eJumpStatus newjump )
{

   if ( bob->jump_status == GROUND ) // From ground position  ...
   {
      if ( newjump == PREPARE ) // ... to landing position
      {
         if ( bob->move_direction == LEFT )
            AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_JUMP_LEFT);
         else if ( bob->move_direction == RIGHT )
            AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_JUMP_RIGHT);
         else if ( bob->move_direction == STAND )
            AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_JUMP_STAND);

         AnimatedSpritesheet_run_at( bob->sheet, 0 );
      }
      else if ( newjump == IN_AIR ) // ... falling in air
      {
         AnimatedSpritesheet_stop( bob->sheet);
         bob->sheet->currentColumn = BOB_ANIMATION_LAND_FRAME - 1;
         // set the in air animation frame. 1 frame before starting the landing procedure
      }
      else if ( newjump == NO_JUMP ) // ... to moving into air
      {
         if ( bob->move_direction != newdir )
         {
            if ( newdir == LEFT )
               AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_MOVE_LEFT);
            else if ( newdir  == RIGHT )
               AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_MOVE_RIGHT);
            else if ( newdir == STAND )
               AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_STAND);
         }
      }
   }
   else if ( bob->jump_status == IN_AIR ) // From in air position ...
   {
      if ( newjump == LANDING ) // ... to landing position
      {
         AnimatedSpritesheet_run_at( bob->sheet, BOB_ANIMATION_LAND_FRAME );
      }
      else if ( newjump == NO_JUMP ) // ... to moving into air
      {
         if ( newdir == LEFT )
         {
            AnimatedSpritesheet_setRow_only( bob->sheet, BOB_ROW_JUMP_LEFT);
         }
         else if ( newdir == RIGHT )
         {
            AnimatedSpritesheet_setRow_only( bob->sheet, BOB_ROW_JUMP_RIGHT);
         }
         else if ( newdir == STAND )
         {
            AnimatedSpritesheet_setRow_only( bob->sheet, BOB_ROW_JUMP_STAND);
         }
      }
   }
   else if ( bob->jump_status == PREPARE ) // From Preparing to jump ...
   {
      if ( newjump == IN_AIR ) // ... to in air position
      {
         AnimatedSpritesheet_stop( bob->sheet);
      }
   }
   else if ( bob->jump_status == LANDING ) // From Landing position ...
   {
      if ( newjump == GROUND ) // ... to ground position
      {
         if ( bob->move_direction == LEFT )
            AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_MOVE_LEFT);
         else if ( bob->move_direction == RIGHT )
            AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_MOVE_RIGHT);
         else if ( bob->move_direction == STAND )
            AnimatedSpritesheet_setRow( bob->sheet, BOB_ROW_STAND);

         AnimatedSpritesheet_run_at(bob->sheet,  0);
      }
   }

   //NOTE: Force changing the status even if the animation is not modified
   if ( newdir != NO_MOVE ) bob->move_direction = newdir;
   if ( newjump != NO_JUMP ) bob->jump_status = newjump;

}

/**
 * Handle movement of the sprite on the horizontal axis
 *
 * @param bob   Sprite to modify
 * @param map   Map to use for stage collision
*/

void move_sprite_horizontally ( sBobSprite *bob, struct Map *map )
{
   int elapsed = SDL_GetTicks() - bob->move_last_update;
   int ticks_per_pixel = ( 1000 / BOB_MOVE_PIXEL_PER_SECOND );
   int nb_pixel = elapsed / ticks_per_pixel;

   while ( nb_pixel > 0)
   {
      if ( bob->jump_status == GROUND || bob->jump_status == IN_AIR )
      {
         if ( bob->move_direction == LEFT )
         {  // limit movement to screen edge
            if ( bob->bounding_box.x > 0 )
            {
               if ( bob_collide_with_stage( bob, map, SIDE_LEFT ) == false )
                  { bob->bounding_box.x -= 1; }
            }
         }
         else if ( bob->move_direction == RIGHT )
         {  // limit movement to screen edge
            if ( bob->bounding_box.x + bob->bounding_box.w < SCREEN_WIDTH )
            {
               if ( bob_collide_with_stage( bob, map, SIDE_RIGHT ) == false )
                  { bob->bounding_box.x += 1; }
            }
         }
      }
      bob->move_last_update += ticks_per_pixel;
      nb_pixel--;
   }
}

/**
 * Handle movement of the sprite on the vertical axis
 *
 * @param bob   Sprite to modify
 * @param map   Map to use for stage collision
*/


void move_sprite_vertically ( sBobSprite *bob, struct Map *map )
{
   int elapsed = SDL_GetTicks() - bob->jump_last_update;
   int ticks_per_pixel = ( 1000 / abs ( bob->y_speed ) );
   int nb_pixel = elapsed / ticks_per_pixel;
   bool land = false;

   while ( nb_pixel > 0)
   {
      if ( bob->y_speed < 0 )
      {
         if ( bob_collide_with_stage( bob, map, SIDE_UP ) == false )
            { bob->bounding_box.y -= 1; }
         else
            { bob->y_speed = BOB_DROP_BASE_SPEED; } // force a drop since collide with celing
      }
      else // y_speed > 0
      {
         land = false;
         if ( bob->bounding_box.y + bob->bounding_box.h < SCREEN_HEIGHT )
         {

            if ( bob_collide_with_stage( bob, map, SIDE_DOWN ) == false )
               { bob->bounding_box.y += 1; }
            else
               { land = true; }
         }
         else { land = true; }

         if ( land == true )
         {
            bob->y_speed = 0;
            bob->jump_delay = SDL_GetTicks() + BOB_JUMP_LAND_DELAY;
            change_animation_sequence( bob, NO_MOVE, LANDING );
         }
      }

      //NOTE: Movement change will not match perfectly with gravity, but good enough
      bob->time_at_same_speed += ticks_per_pixel;
      bob->jump_last_update += ticks_per_pixel;
      nb_pixel -= 1;
   }
   //NOTE: is movement is more than 1 pixel and there is a stage collision,
   // it will try to repeat the movement and check for collision until nb_pixel = 0
   // it's necessary to keep the timing accurate.

   //---  Modifies the jump translation speed as time progress ---

   //NOTE: if the game lags, bob could move a few pixel faster because
   //the numbers of pixels to move is not adjusted during movement.
   if ( bob->time_at_same_speed > BOB_JUMP_CHANGE_SPEED )
   {
      if ( bob->y_speed < 0)
      {
         if ( bob->y_speed < BOB_JUMP_THRESHOLD_SPEED )
         {  bob->y_speed /= 2; }
         else
         {  bob->y_speed = BOB_DROP_BASE_SPEED; }
         bob->time_at_same_speed = 0;
      }
      else if ( bob->y_speed < 1000 )
      {  bob->y_speed *= 2;
         bob->time_at_same_speed = 0;
         if ( bob->y_speed >= 1000 ) bob->y_speed = 1000;
         // capped to 1000 because create a floating point exception with division
         // for  variable ticks_per_pixel
      }
   }
}

//----------------------
// Public Methods
//----------------------

sBobSprite *bob_create ( const sResources *res, int height )
{
   sBobSprite *b = (sBobSprite*) malloc ( sizeof (sBobSprite));
   b->sheet = AnimatedSpritesheet_create( res->spr_bob, BOB_ANIMATION_DELAY );

   b->bounding_box.x = 0;
   b->bounding_box.h = height;
   b->bounding_box.w = (float)height / BOB_BOX_WIDTH_RATIO;
   b->bounding_box.y = SCREEN_HEIGHT - b->bounding_box.h;
   b->sprite_box.h = ((float)height) * BOB_SPRITE_HEIGHT_RATIO;
   b->sprite_box.w = b->sprite_box.h;
   b->sprite_box.x = ( b->sprite_box.w - b->bounding_box.w ) / 2;
   b->sprite_box.y = b->sprite_box.h - b->bounding_box.h;
   b->move_direction = STAND;
   b->move_last_update = SDL_GetTicks();
   b->jump_last_update = SDL_GetTicks();
   b->jump_status = GROUND;
   b->time_at_same_speed = 0;
   b->y_speed = 0;
   b->jump_delay = 0;
   AnimatedSpritesheet_setRow( b->sheet, BOB_ROW_STAND );

   AnimatedSpritesheet_run( b->sheet );

   //NOTE: set to false to remove debugging rectangle
   b->debug_rect = false;
   return b;
}

void bob_render ( sBobSprite *bob, struct Map *map )
{

   move_sprite_horizontally( bob, map );

   if ( bob->y_speed != 0)
   {
      move_sprite_vertically( bob, map );
   }

   // Trigger land and jumps after delay exceeded
   if ( bob->jump_status == PREPARE && bob->jump_delay < SDL_GetTicks() )
   {
      bob->y_speed = BOB_JUMP_PIXEL_PER_SECOND;
      bob->time_at_same_speed = 0;
      bob->jump_last_update = SDL_GetTicks();
      change_animation_sequence( bob, NO_MOVE, IN_AIR );
   }
   else if ( bob->jump_status == LANDING && bob->jump_delay < SDL_GetTicks() )
   {
      change_animation_sequence( bob, NO_MOVE, GROUND );
   }
   else if ( bob->jump_status == GROUND
      && bob_collide_with_stage( bob, map, SIDE_DOWN ) == false )
   {
      //Trigger a force drop if fall from a ledge
      bob->time_at_same_speed = 0;
      bob->jump_last_update = SDL_GetTicks();
      change_animation_sequence( bob, NO_MOVE, IN_AIR );
      bob->y_speed = BOB_DROP_BASE_SPEED;
   }


   // --- Sprite rendering and animation update ---
      AnimatedSpritesheet_render_size( bob->sheet,
      bob->bounding_box.x - bob->sprite_box.x,
      bob->bounding_box.y - bob->sprite_box.y,
      bob->sprite_box.w, bob->sprite_box.h );
   //NOTE: Need to use the real sprite coordinates since bob is localised
   //according to it's bounding box. The sprite box is adjusted according to the
   //desired final size of the sprite

   //draws a rectangle around bob's bounding box for collision debugging
   if ( bob->debug_rect == true)
   {
      SDL_SetRenderDrawColor(bob->sheet->spritesheet->renderer, 0, 255, 0, 255);
      SDL_RenderDrawRect( bob->sheet->spritesheet->renderer, &bob->bounding_box );
      SDL_SetRenderDrawColor(bob->sheet->spritesheet->renderer, 0, 0, 0, 255);
   }
}

void bob_delete ( sBobSprite *bob )
{
   if ( bob != NULL)
   {
      AnimatedSpritesheet_delete( bob->sheet);
      free ( bob );
   }
}

void bob_set_position ( sBobSprite *bob, int x, int y )
{
   bob->bounding_box.x = x;
   bob->bounding_box.y = y;
}



void bob_give_input ( sBobSprite *bob, SDL_Event *event )
{
   if (event->type == SDL_KEYDOWN )
   {
      if ( event->key.keysym.scancode == SDL_SCANCODE_LEFT )
      {
         if ( bob->jump_status == GROUND || bob->jump_status == IN_AIR )
         {
            change_animation_sequence( bob, LEFT, NO_JUMP);
         }
      }
      else if ( event->key.keysym.scancode == SDL_SCANCODE_RIGHT)
      {
         if ( bob->jump_status == GROUND || bob->jump_status == IN_AIR)
         {
            change_animation_sequence( bob, RIGHT, NO_JUMP );
         }
      }
      else if ( event->key.keysym.scancode == SDL_SCANCODE_UP )
      {
         if ( bob->jump_status == GROUND )
         {
            bob->jump_delay = SDL_GetTicks() + BOB_JUMP_PREPARE_DELAY;
            change_animation_sequence( bob, NO_MOVE, PREPARE );
         }
      }
   }
   else if ( event->type == SDL_KEYUP )
   {
      if ( event->key.keysym.scancode == SDL_SCANCODE_LEFT
         || event->key.keysym.scancode == SDL_SCANCODE_RIGHT )
      {
         change_animation_sequence( bob, STAND, NO_JUMP);
      }
   }
}
