/**
 * System specific methods
 */

#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED

#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

//Map texture info placed here to avoid recursive inclusion of .h
#define MAP_TEXTURE_WIDTH  2
#define MAP_TEXTURE_HEIGHT 2
#define MAP_TEXTURE_FILENAME "assets/stagetexture.png"



typedef struct
{
   SDL_Window *win;
   SDL_Renderer *ren;
}sApplication;

typedef struct
{
   SDL_Texture *tex_title;
   TTF_Font *fnt_donut64;
   TTF_Font *fnt_dunkin64;
   TTF_Font *fnt_dunkin32;
   sBitmapFont *bmf_dunkin64pink;
   struct Spritesheet *spr_donut;
   struct Spritesheet *spr_bob;
   struct Spritesheet *spr_stage;
   struct Map *map;
   struct Spritesheet *spr_score;
}
sResources;


/**
 * Initialise the library
 * @return false if the initialisation failed
 */
bool initialise_SDL ();

/**
 * Close the library
 */
void close_SDL ();

/**
 * Load the necessary resources
 * @param res structure that will contain the loaded resources
 * @return    false if the resources failled to be created
 */
bool load_resources ( sResources *res, const sApplication *app);

/*
 * Free the memory of the loaded resources
 * @param res Structure of resource to free from the memory
 */
void unload_resources ( sResources *res);

/**
 * Create the window and the renderer
 * @param app Structure that will contain the created application
 * @return    false if there was a failure at application creation
 */

bool create_application ( sApplication *app );

/**
 * Free the memory for the application
 * @param app Structure to be removed from memory
 */

void destroy_application ( sApplication *app );

#endif // SYSTEM_H_INCLUDED
