/**
* Sprite that handle the donuts of the game. It Encapsulate and animated
* spritesheet. It could hold later donuts of different colors, where each
* animation row is a different color.
*/

#ifndef DONUT_H_INCLUDED
#define DONUT_H_INCLUDED


#define DONUT_ANIMATION_DELAY 125 // 1/8th of seconds
#define DONUT_ANIMATION_PAUSE 2000 // 2 seconds
#define DONUT_ANIMATION_INTERVAL ( DONUT_ANIMATION_PAUSE / 9)
// makes sure donut does not bounce at the same time.
// Multiply value by i when cleating donuts.
#define DONUT_JUMP_INCREMENT  4
// Number of pixel to move during animation. Since this is a pixel based value,
// it does not consider the scale of the donut. Therefore will jump higher if smaller

#define DONUT_NB_COLS         8
#define DONUT_NB_ROWS         9
#define DONUT_FILENAME        "assets/donut.png"

typedef struct
{
   struct AnimatedSpritesheet  *sheet;
   SDL_Rect                     position; // h w represents sprite size
   SDL_Rect                     destination; //only x y are used
   int                          speed;
   int                          last_update;
   int                          bounce_delay;
   int                          nbDonut;
   bool                         isVisible;
   int                          bounce_offset; // on the y axis
   int                          bounce_timer;
   int                          bounce_last_frame; // last animation frame seen
   bool                         debug_rect;
}sDonutSprite;
//NOTE: last_update will hold complete pixel movement to avoid keeping into
// a second variable a remainder to time which was not used yet for movement.


/**
* Allow the creation of donut sprites. Donut colors will be implemented at the end
*
* @param color           use a different animation row
* @param animation_delay amount of time to wait before first donut bounce.
*                        It makes sure not all donuts bounce at the same time
* @param draw_width      Desired width to be drawn on the screen.
* @param draw_height     Desired height to be drawn on the screen.
* @return a structure for a new donut
*/

sDonutSprite *donut_create ( const sResources *res, int color, int animation_delay,
                              int draw_width, int draw_height );

/**
* Update the position and render the sprite on the screen. The method will
* query time by itself to perform movement.
*
* @param sprite sprite to change
*/
void donut_render (sDonutSprite *donut);

/**
* Update the position and render the donut on the screen. The method will
* query time by itself to perform movement.
*
* NOTE: speed is applied equally to the X and Y axis, which means that moving
* in diagonal will be faster. Mainlyused in the menu and for bouncing. So
* there is no diagonal movement.
*
* @param sprite  sprite to change
* @param dst_x   destination X to move to
* @param dst_y   destination Y to move to
* @param speed   Movement speed in pixels per seconds (max=1000)
*/
void donut_moveto (sDonutSprite *donut, int dst_x, int dst_y , int speed );

/**
* Set an initial position to the object to avoid movement to that destination.
* Could also be used to teleport a donut.
*
* @param sprite sprite to change
* @param x      sprite X position
* @param y      sprite Y position
*/
void donut_set_position ( sDonutSprite *donut, int x, int y );

/**
* Free the memory used by donut. Even if donut does not use pointer it
* contains an animated spritesheet that is allocated dynamically.
*
* @param donut structure to free from memory
*/
void donut_delete(sDonutSprite *donut);

#endif // DONUT_H_INCLUDED
