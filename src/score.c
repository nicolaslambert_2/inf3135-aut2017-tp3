#include "font.h"
#include "system.h"
#include "map.h"
#include "score.h"

// ----------------------
// private method
// ----------------------

/**
 * returns the position in the answer of the desired donut
 *
 * @param strack     The scoring track to search
 * @param donut_id   the number of the donut to search
 * @return the array positin of that donut. If there is an error, will return
 * the number of donuts in the track.
 */

int find_donut ( sScoreTrack *strack, int donut_id )
{
   int i = 0;
   while ( strack->answer_sequence [ i ] != donut_id && i < strack->nb_donut )
      { i++; }

   return i;
}

// ----------------------
// Public Methods
// ----------------------

sScoreTrack *scoretrack_create ( int nb_donut )
{
   sScoreTrack *s = (sScoreTrack*) malloc ( sizeof ( sScoreTrack));
   s->nb_donut = nb_donut;
   return s;
}

void scoretrack_reset ( sScoreTrack *strack )
{
   int i;
   int position;


   strack->collect_index = 0;

   // empty arrays first
   for ( i = 0 ; i < MAX_STAGE_DONUTS ; i++)
   {
      strack->answer_sequence [ i ] = -1;
      strack->collect_sequence [ i ] = -1;
   }

   // allocate donuts randomly
   for ( i = 0 ; i < strack->nb_donut ; i++)
   {
      position = rand() % strack->nb_donut;
      while ( strack->answer_sequence [ position ] != -1)
      {
         position++;
         if ( position >= strack->nb_donut ) position = 0;
      }
      strack->answer_sequence [ position ] = i;
   }
}

void scoretrack_add ( sScoreTrack *strack, int donut_id )
{
   if ( strack->collect_index < strack->nb_donut )
   {
      strack->collect_sequence [ strack->collect_index ] = donut_id;
      strack->collect_index++;
   }
}

int scoretrack_last ( sScoreTrack *strack )
{
   if ( strack->collect_index == 0)
      { return -1; } // there is no last donut
   else
      { return ( strack->collect_sequence [ strack->collect_index - 1] ); }
}


int scoretrack_get_score ( sScoreTrack *strack )
{
   int i;
   int answer;
   int previous_answer = -1;
   int score = 0;
   int consecutive = 1; // number of consecutive donut

   for ( i = 0 ; i < strack->nb_donut ; i++)
   {
      answer = find_donut( strack, strack->collect_sequence [ i ]);

      if ( previous_answer != -1 ) // skip the first pass
      {  if ( answer - previous_answer == 1) // they are consecutive in the answer
         {  consecutive++;
         }
         else
         {  consecutive = 1;
         }
      }

      score += consecutive * consecutive;
      previous_answer = answer;
   }

   return score;
}

bool scoretrack_get_chain ( sScoreTrack *strack, int index )
{
   int answer_a = find_donut( strack, strack->collect_sequence [ index ] );
   int answer_b = find_donut( strack, strack->collect_sequence [ index + 1] );

   return (( answer_b - answer_a) == 1 ); // they are consecutive answer
}

void scoretrack_delete ( sScoreTrack *strack )
{
   if ( strack != NULL )
      { free (strack); }
}

