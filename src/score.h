/*
 * Handle the scoring system with the sequence of donuts to collect
 */
#ifndef SCORE_H_INCLUDED
#define SCORE_H_INCLUDED

#define SCORE_TEX_NBROWS    1
#define SCORE_TEX_NBCOLS    4

#define SCORE_FRAME     0
#define SCORE_FAILED    1
#define SCORE_SUCCESS   2
#define SCORE_ARROW     3

#define SCORE_TEXTURE_FILENAME "assets/scoretile.png"

typedef struct
{
   int nb_donut; // nb of donuts in the scoring track
   int answer_sequence [ MAX_STAGE_DONUTS ]; // target donut sequence
   int collect_sequence [ MAX_STAGE_DONUTS ]; // collected donut sequence
   int collect_index; // position to place the new donut
}sScoreTrack;

/**
 * Create and initialise randomly all the donuts in the answer while clearing
 * the rest.
 *
 * WARNING: scoretrack_reset() must be called after the creation
 *
 * @param nb_donut   The number of donuts
 * @return a dynamically allocated sScoreTrack
 */

sScoreTrack *scoretrack_create ( int nb_donut );

/**
 * Reset all the values of the object when startin a new game
 *
 * @param strack   Track to reset
 * @param nb_donut nuber of donuts to generate
 */

void scoretrack_reset ( sScoreTrack *strack );

/**
 * add a donut to the list of collected donuts
 *
 * @param strack    track to add to
 * @param donut_id  identification number of the donut to add
*/
void scoretrack_add ( sScoreTrack *strack, int donut_id );

/**
 * Get the ID of the last collected donuts for display on screen
 *
 * @param strack    track to query
 * @return ID of the last donut
*/
int scoretrack_last ( sScoreTrack *strack );

//bool scoretrack_iscomplete ( sScoreTrack *strack );

/**
 * Get the final score once all donuts are collected
 *
 * @param strack    track to calculate the score
 * @return the final score
*/
int scoretrack_get_score ( sScoreTrack *strack );

/**
 * Determine if the link between 2 consecutive selected donut is a valid chain
 * in order to display success or failure on the victory screen. Consult the
 * test code to get an example of how it is used. You need to iterate 1 less
 * time than the number of donuts since there is less connections than donuts
 *
 * @param strack    track to get the chain from
 * @return true if the chain is valid, else false.
*/
bool scoretrack_get_chain ( sScoreTrack *strack, int index );

/**
 * destroy a sScoreTrack object
 *
 * @param strack     ScoreTrack to destroy
 */
void scoretrack_delete ( sScoreTrack *strack );


#endif // SCORE_H_INCLUDED
