#include <stdio.h>
#include "font.h"
#include "system.h"
#include "map.h"
#include "score.h"
#include "screen.h"
#include "spritesheet.h"
#include "animated_spritesheet.h"
#include "donut.h"
#include "test.h"
#include "sdlhelper.h"
#include "bob.h"



eScreen title_screen_mainloop ( const sApplication *app, const sResources *res )
{
   SDL_Rect pos_title;
   SDL_Rect pos_menu_anchor;
   SDL_Color fnt_color = { .a= 255, .r=255, .g=150, .b=200 };
   SDL_Color bgcolor = {.r=41, .g=31, .b=19, .a=255 };
   SDL_Texture *texmsg_menu[NB_MENU];
   SDL_Event evn;
   bool quit = false;
   int menu_cursor = 0;
   int old_cursor = 0;
   int menu_x_position = 0;
   //int menu_y_difference = 0;
   int i;
   eScreen exit_code = EXIT_GAME;

   // precalculate image position of title and menu to save time

   SDL_QueryTexture( res->tex_title, NULL, NULL, &pos_title.w, &pos_title.h );
   pos_title.x = (SCREEN_WIDTH / 2) - (pos_title.w / 2);
   pos_title.y = (SCREEN_HEIGHT / 4) - ( pos_title.h / 2 );


   texmsg_menu[0] = textmessage_create( "Start Game", res->fnt_donut64,
      fnt_color, BLENDED, app->ren );
   texmsg_menu[1] = textmessage_create( "Quit", res->fnt_donut64,
      fnt_color, BLENDED, app->ren );

   SDL_QueryTexture( texmsg_menu[0], NULL, NULL, &pos_menu_anchor.w, &pos_menu_anchor.h );
   pos_menu_anchor.x = (SCREEN_WIDTH / 2) - ( pos_menu_anchor.w /2 );
   pos_menu_anchor.y = ((SCREEN_HEIGHT / 4) * 3 ) - pos_menu_anchor.h;
   //NOTE: the first menu item is the largest item in the menu, so we use it

   menu_x_position = pos_menu_anchor.x - (pos_menu_anchor.h + 8 );



   sDonutSprite *donut = donut_create ( res, 0, 0, pos_menu_anchor.h, pos_menu_anchor.h );

   donut_set_position( donut, menu_x_position, pos_menu_anchor.y );

   while ( quit == false )
   {
      // ---------- Read input ----------

      while (SDL_PollEvent(&evn) == 1)
      {
         if (evn.type == SDL_QUIT) quit = true;
         if (evn.type == SDL_KEYDOWN )
         {
            old_cursor = menu_cursor;
            if ( evn.key.keysym.scancode == SDL_SCANCODE_ESCAPE ) quit = true;
            else if ( evn.key.keysym.scancode == SDL_SCANCODE_UP )
            {
               if (menu_cursor > 0 )
               {
                  menu_cursor--;

               }
            }
            else if ( evn.key.keysym.scancode == SDL_SCANCODE_DOWN )
            {
               if (menu_cursor < 1 )
               {
                  menu_cursor++;

               }
            }
            else if ( evn.key.keysym.scancode == SDL_SCANCODE_RETURN )
            {
               if ( menu_cursor == 1 )
               {
                   quit = true;
                   exit_code = EXIT_GAME;
               }
               else if ( menu_cursor == 0)
               {
                  quit = true;
                  exit_code = READY_SCREEN;
               }
            }
            //NOTE: I am not using a switch statement to optimise speed

            if ( old_cursor != menu_cursor )
            {
               donut_moveto( donut, menu_x_position, pos_menu_anchor.y
                  + (pos_menu_anchor.h * menu_cursor) , MENU_CURSOR_SPEED  );
            }
         }
		}

      // ---------- Render ----------
      // NOTE: The update of the position and animation are made inside the render
      // functions of sprite.

      drawBackground( res->spr_stage, app->ren, 0, 0, bgcolor, 8 );

      SDL_RenderCopy ( app->ren, res->tex_title, NULL, &pos_title );

      for ( i = 0 ; i < NB_MENU ; i++)
      {
         texture_render( texmsg_menu[i], pos_menu_anchor.x,
            pos_menu_anchor.y + i * pos_menu_anchor.h, app->ren );
      }

      donut_render( donut );

      SDL_RenderPresent ( app->ren );
   }


   return exit_code;
}


eScreen game_screen_mainloop ( const sApplication *app, const sResources *res,
                               sScoreTrack *score )
{
  bool quit = false;
  int i = 0;
  int k = 0;
  int j = 0;
  SDL_Color bgcolor = {.r=231, .g=156, .b=66, .a=255 };
  int exit_code = SCORE_SCREEN;

  struct Map *map = res->map;

  sBobSprite *bob = bob_create( res, (MAP_BLOCK_SIZE * 2) -2 );

  sDonutSprite *donuts[9];

  int nbDonutPicked = 0;

  //Create all the donuts
  int halfblock = MAP_BLOCK_SIZE / 2;
  int quarterblock = MAP_BLOCK_SIZE / 4;
  for (j = 0; j < 9; j++) {
    donuts[j] = donut_create ( res, j, j * DONUT_ANIMATION_INTERVAL
    , MAP_BLOCK_SIZE + halfblock, MAP_BLOCK_SIZE + halfblock );
  }

  //Hardcode position of the donuts in the level

  donut_set_position( donuts[0], 3*MAP_BLOCK_SIZE - halfblock, 1*MAP_BLOCK_SIZE - halfblock );
  donut_set_position( donuts[1], 9*MAP_BLOCK_SIZE + quarterblock, 3*MAP_BLOCK_SIZE - halfblock);
  donut_set_position( donuts[2], 16*MAP_BLOCK_SIZE , 1*MAP_BLOCK_SIZE - halfblock);
  donut_set_position( donuts[3], 1*MAP_BLOCK_SIZE - halfblock, 5*MAP_BLOCK_SIZE - halfblock);
  donut_set_position( donuts[4], 18*MAP_BLOCK_SIZE , 5*MAP_BLOCK_SIZE - halfblock);
  donut_set_position( donuts[5], 9*MAP_BLOCK_SIZE + quarterblock, 8*MAP_BLOCK_SIZE - halfblock);
  donut_set_position( donuts[6], 2*MAP_BLOCK_SIZE , 9*MAP_BLOCK_SIZE - halfblock);
  donut_set_position( donuts[7], 16*MAP_BLOCK_SIZE + halfblock, 9*MAP_BLOCK_SIZE - halfblock);
  donut_set_position( donuts[8], 9*MAP_BLOCK_SIZE + quarterblock, 12*MAP_BLOCK_SIZE - halfblock);

  bob_set_position( bob, MAP_BLOCK_SIZE * 3, SCREEN_HEIGHT - (MAP_BLOCK_SIZE * 3) +1 );

  // animation loop
  while (!quit)
  {
    // process events
    SDL_Event evn;
    while (SDL_PollEvent(&evn) == 1)
    {
      if (evn.type == SDL_QUIT)
      {  quit = true;
         exit_code = EXIT_GAME;
      }
      if ( evn.key.keysym.scancode == SDL_SCANCODE_ESCAPE )
        {  exit_code = TITLE_SCREEN;
           quit = true;
        }
      bob_give_input( bob, &evn );
    }

    //Map
    drawBackground( res->spr_stage, app->ren, 1, 2, bgcolor, 10 );
    drawMap(app,res);

    //Donut Colision detectiony
    for(i = 0; i < 9; i++)
    {
      if(SDL_HasIntersection(&bob->bounding_box, &donuts[i]->position) && donuts[i]->isVisible == true)
      {
        donuts[i]->isVisible = false;
        nbDonutPicked += 1;
        scoretrack_add ( score, i );
      }
    }

    for (k = 0; k < MAX_STAGE_DONUTS; k++) {
      donut_render(donuts[k]);
    }


    if (nbDonutPicked == 9) {
      exit_code == SCORE_SCREEN;
      quit = true;
    }

    //Draw the character
    bob_render( bob , map );

    // HUD with the last donut

    Spritesheet_render_size( res->spr_score, quarterblock, quarterblock, SCORE_FRAME,
       MAP_BLOCK_SIZE * 2, MAP_BLOCK_SIZE * 2 );

    if ( scoretrack_last ( score ) != -1 )
    {  Spritesheet_render_size( res->spr_donut, quarterblock + 4, quarterblock + 2,
            scoretrack_last (score) * 8 /*nb animation frame*/, 56, 56 );
    }

    //Push the image to the screen
    SDL_RenderPresent(app->ren);
    // wait 1/60th of a second
    SDL_Delay(1000/60);

  }

  bob_delete(bob);

  return exit_code;
}


eScreen ready_screen_mainloop ( const sApplication *app, const sResources *res,
                                 sScoreTrack *score )
{
   int exit_code = GAME_SCREEN;
   bool quit = false;
   SDL_Event evn;

   SDL_Color fnt_color = { .a= 255, .r=255, .g=150, .b=200 };
   SDL_Color bgcolor = {.r=41, .g=31, .b=19, .a=255 };

   int x_clock_pos = ( SCREEN_WIDTH - bitmapfont_get_width( res->bmf_dunkin64pink ) ) / 2;
   SDL_Texture *msg_ready = textmessage_create("Ready", res->fnt_dunkin64 , fnt_color, BLENDED, app->ren);
   SDL_Texture *msg_collect = textmessage_create("Collect Donuts in order", res->fnt_dunkin32 , fnt_color, BLENDED, app->ren);
   int x_ready_width;
   int x_collect_width;
   SDL_QueryTexture( msg_ready, NULL, NULL, &x_ready_width, NULL);
   SDL_QueryTexture( msg_collect, NULL, NULL, &x_collect_width, NULL);
   int x_ready_pos = ( SCREEN_WIDTH - x_ready_width ) / 2;
   int x_collect_pos = ( SCREEN_WIDTH - x_collect_width ) / 2;

   int starticks = SDL_GetTicks();
   int endticks = starticks + 9000; // give the player 9 seconds
   int currenticks = starticks;

   SDL_SetRenderDrawColor(app->ren, 0, 0, 0, 128);
   while ( quit == false && currenticks < endticks)
   {
      while (SDL_PollEvent(&evn) == 1)
      {
        if (evn.type == SDL_QUIT)
        {  quit = true;
           exit_code = EXIT_GAME;
        }
        if (evn.type == SDL_KEYDOWN )
        {  if ( evn.key.keysym.scancode == SDL_SCANCODE_ESCAPE )
           {  quit = true;
              exit_code = TITLE_SCREEN;
           }
        }
      }

      drawBackground( res->spr_stage, app->ren, 0, 0, bgcolor, 8 );
      bitmapfont_render( res->bmf_dunkin64pink,
         10 - ((( currenticks - starticks ) / 1000 ) + 1), x_clock_pos, 100, app->ren );

      texture_render( msg_ready, x_ready_pos, 0, app->ren );
      texture_render( msg_collect, x_collect_pos, SCREEN_HEIGHT - 100 , app->ren );

      //drawing donut area
      SDL_Rect rect = { .x=0, .y=200, .w=64, .h=64 };
      int i;

      for ( i = 0 ; i < MAX_STAGE_DONUTS ; i++)
      {
         Spritesheet_render_size( res->spr_score, rect.x, rect.y, SCORE_FRAME, rect.w, rect.h );
         Spritesheet_render_size( res->spr_donut, rect.x + 4, rect.y + 2,
            score->answer_sequence [ i ] * 8 /*nb animation frame*/, rect.w - 8, rect.h - 8 );
         rect.x += 64 + 8;
      }

      rect = (SDL_Rect) { .x=36, .y=200, .w=64, .h=64 };

      for ( i = 0 ; i < MAX_STAGE_DONUTS - 1 ; i++)
      {
         Spritesheet_render_size( res->spr_score, rect.x, rect.y, SCORE_ARROW, rect.w, rect.h );
         rect.x += 64 + 8;
      }


      SDL_RenderPresent ( app->ren );
      currenticks = SDL_GetTicks();
   }
   SDL_SetRenderDrawColor(app->ren, 0, 0, 0, 255);

   return exit_code;
}

eScreen score_screen_mainloop ( const sApplication *app, const sResources *res,
  sScoreTrack *score)
  {
    int i,j;
    int nb_donut = 9;
    bool quit = false;
    int exit_code = TITLE_SCREEN;

    SDL_Color fnt_color = { .a= 255, .r=255, .g=150, .b=200 };
    SDL_Color bgcolor = {.r=41, .g=31, .b=19, .a=255 };

    SDL_Texture *msg_grats = textmessage_create("Results!", res->fnt_dunkin64 , fnt_color, BLENDED, app->ren);
    SDL_Texture *msg_collect = textmessage_create("The sequance was", res->fnt_dunkin32 , fnt_color, BLENDED, app->ren);
    SDL_Texture *msg_did = textmessage_create("Your collect order was", res->fnt_dunkin32 , fnt_color, BLENDED, app->ren);
    SDL_Texture *msg_score = textmessage_create("Score: ", res->fnt_dunkin64 , fnt_color, BLENDED, app->ren);

    int x_score_pos = ( SCREEN_WIDTH - bitmapfont_get_width( res->bmf_dunkin64pink ) ) / 2;

    int x_grats_width;
    int x_collect_width;
    int x_did_width;
    int x_score_width;
    SDL_QueryTexture( msg_grats, NULL, NULL, &x_grats_width, NULL);
    SDL_QueryTexture( msg_collect, NULL, NULL, &x_collect_width, NULL);
    SDL_QueryTexture( msg_did, NULL, NULL, &x_did_width, NULL);
    SDL_QueryTexture( msg_score, NULL, NULL, &x_score_pos, NULL);
    int x_grats_pos = ( SCREEN_WIDTH - x_grats_width ) / 2;
    int x_collect_pos = ( SCREEN_WIDTH - x_collect_width ) / 2;
    int x_did_pos = ( SCREEN_WIDTH - x_did_width ) / 2;

    while (!quit)
    {
      // process events
      SDL_Event evn;
      while (SDL_PollEvent(&evn) == 1)
      {
        if (evn.type == SDL_QUIT)
        {  quit = true;
           exit_code = EXIT_GAME;
        }
        if ( evn.type == SDL_KEYDOWN )
        {
           if ( evn.key.keysym.scancode == SDL_SCANCODE_ESCAPE )
           {  quit = true;
              exit_code = TITLE_SCREEN;
           }
        }
      }

      drawBackground( res->spr_stage, app->ren, 0, 0, bgcolor, 8 );

      bitmapfont_render( res->bmf_dunkin64pink,
         scoretrack_get_score ( score ) , x_score_pos + 150, SCREEN_HEIGHT - 100, app->ren );

      texture_render( msg_grats, x_grats_pos, 0, app->ren );
      texture_render( msg_collect, x_collect_pos, 125 , app->ren );
      texture_render( msg_did, x_did_pos, 250 , app->ren );
      texture_render( msg_score, x_score_pos - 150, SCREEN_HEIGHT - 100, app->ren );

      //Drawing the answer donut area
      SDL_Rect rect1 = { .x=0, .y=175, .w=64, .h=64 };
      for ( i = 0 ; i < MAX_STAGE_DONUTS ; i++)
      {
         Spritesheet_render_size( res->spr_score, rect1.x, rect1.y, SCORE_FRAME, rect1.w, rect1.h );
         Spritesheet_render_size( res->spr_donut, rect1.x + 4, rect1.y + 2,
            score->answer_sequence [ i ] * 8 /*nb animation frame*/, rect1.w - 8, rect1.h - 8 );
         rect1.x += 64 + 8;
      }

      rect1 = (SDL_Rect) { .x=36, .y=175, .w=64, .h=64 };

      for ( i = 0 ; i < MAX_STAGE_DONUTS - 1 ; i++)
      {
         Spritesheet_render_size( res->spr_score, rect1.x, rect1.y, SCORE_ARROW, rect1.w, rect1.h );
         rect1.x += 64 + 8;
      }

      //Drawing the user response donut area
      SDL_Rect rect2 = { .x=0, .y=300, .w=64, .h=64 };

      for ( j = 0 ; j < MAX_STAGE_DONUTS ; j++)
      {
        Spritesheet_render_size( res->spr_score, rect2.x, rect2.y, SCORE_FRAME, rect2.w, rect2.h );
        Spritesheet_render_size( res->spr_donut, rect2.x + 4, rect2.y + 2,
          score->collect_sequence [ j ] * 8 /*animation frame*/, rect2.w - 8, rect2.h - 8 );
          rect2.x += 64 + 8;
        }

        rect2 = (SDL_Rect) { .x=36, .y=300, .w=64, .h=64 };

        for ( j = 0 ; j < MAX_STAGE_DONUTS - 1 ; j++)
        {
          if ( scoretrack_get_chain ( score, j ) == true )
          {
            Spritesheet_render_size( res->spr_score, rect2.x, rect2.y, SCORE_SUCCESS, rect2.w, rect2.h );
            rect2.x += 64 + 8;
          }
          else
          {
            Spritesheet_render_size( res->spr_score, rect2.x, rect2.y, SCORE_FAILED, rect2.w, rect2.h );
            rect2.x += 64 + 8;
          }
        }

        SDL_RenderPresent ( app->ren );
        SDL_Delay(1000/30);
      }


  return exit_code;
}

eScreen test_screen_mainloop ( const sApplication *app, const sResources *res )
{
   //test_bob_movement( app, res);
   //test_draw_background( app, res );
   //test_score_track ( app, res );
   return EXIT_GAME;
}
