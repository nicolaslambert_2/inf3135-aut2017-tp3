#include "font.h"
#include "system.h"
#include "spritesheet.h"
#include "animated_spritesheet.h"
#include "donut.h"



//--------------
// Public Methods
//--------------

sDonutSprite *donut_create ( const sResources *res, int color, int animation_delay,
                              int draw_width, int draw_height )
{

   sDonutSprite *s = (sDonutSprite*) malloc (sizeof ( sDonutSprite ));
   s->sheet = AnimatedSpritesheet_create(res->spr_donut, DONUT_ANIMATION_DELAY );
   s->position = (SDL_Rect) { .x=0, .y=0, .h=draw_height, .w=draw_width };
   s->destination = (SDL_Rect) { .x=0, .y=0, .h=0, .w=0 };
   s->isVisible = true; //Make donut visible at creation

   s->speed = 100; // set a default speed to avoid division by 0 in render
   s->last_update = SDL_GetTicks();
   s->bounce_timer = SDL_GetTicks() + animation_delay;
   s->debug_rect = false;
   s->bounce_offset = 0;
   s->bounce_last_frame = 0;
   AnimatedSpritesheet_setRow( s->sheet, color );
   AnimatedSpritesheet_stop( s->sheet );
   return s;
}

void donut_render (sDonutSprite *donut)
{
  if (donut->isVisible == true) {

    int elapsed = SDL_GetTicks() - donut->last_update;
    int ticks_per_pixel = ( 1000 / donut->speed );
    int nb_pixel = elapsed / ticks_per_pixel;
    donut->last_update += nb_pixel * ticks_per_pixel;
    int dif_x = donut->position.x - donut->destination.x;
    int dif_y = donut->position.y - donut->destination.y;

   // --- start/stop animation ---

   if ( donut->last_update > donut->bounce_timer )
   {
      if ( donut->sheet->running == true )
      {
         AnimatedSpritesheet_stop( donut->sheet );
         donut->sheet->currentColumn = 0;
         donut->bounce_timer = donut->last_update + DONUT_ANIMATION_PAUSE;
      }
      else if ( donut->sheet->running == false )
      {
         AnimatedSpritesheet_run( donut->sheet );
         donut->bounce_timer = donut->last_update + (DONUT_ANIMATION_DELAY * 9);
      }
   }

   // --- make donut jump according to specific animation frame --

   if ( donut->bounce_last_frame != donut->sheet->currentColumn)
   {
      donut->bounce_last_frame = donut->sheet->currentColumn;
      if ( donut->sheet->currentColumn == 3 || donut->sheet->currentColumn == 4)
         { donut->bounce_offset += DONUT_JUMP_INCREMENT; }
      else if ( donut->sheet->currentColumn == 5 || donut->sheet->currentColumn == 6)
         { donut->bounce_offset -= DONUT_JUMP_INCREMENT; }
      else if ( donut->sheet->currentColumn == 7 )
         { donut->bounce_offset = 0; } // just to make sure it never goes negative
   }

   // --- update the position of the donut ---
   if ( dif_x != 0 )
   {
      if ( abs(dif_x) < nb_pixel ) nb_pixel = abs(dif_x);
      if ( dif_x < 0 ) donut->position.x += nb_pixel;
      else donut->position.x -= nb_pixel;
    }

    if ( dif_y != 0 )
    {
      if ( abs(dif_y) < nb_pixel ) nb_pixel = abs(dif_y);
      if ( dif_y < 0 ) donut->position.y += nb_pixel;
      else donut->position.y -= nb_pixel;
    }

   // --- render donut and update animation ---
   AnimatedSpritesheet_render_size( donut->sheet, donut->position.x,
      donut->position.y - donut->bounce_offset, donut->position.w, donut->position.h);

      if ( donut->debug_rect == true)
      {
        SDL_SetRenderDrawColor(donut->sheet->spritesheet->renderer, 0, 255, 0, 255);
        SDL_RenderDrawRect( donut->sheet->spritesheet->renderer, &donut->position );
        SDL_SetRenderDrawColor(donut->sheet->spritesheet->renderer, 0, 0, 0, 255);
      }
    }
  }

void donut_moveto (sDonutSprite *donut, int dst_x, int dst_y , int speed )
{
   donut->destination.x = dst_x;
   donut->destination.y = dst_y;
   donut->speed = speed;
   if ( donut->speed > 1000) donut->speed = 1000;

}

void donut_delete(sDonutSprite *donut)
{
   if ( donut != NULL )
   {
      AnimatedSpritesheet_delete( donut->sheet );
      free (donut);
   }
}

void donut_set_position ( sDonutSprite *donut, int x, int y )
{
   donut->position.x = x;
   donut->position.y = y;
   donut->destination.x = x;
   donut->destination.y = y;
}
