/**
 * Animated sprite of the player.
 */

#ifndef BOB_H_INCLUDED
#define BOB_H_INCLUDED

#include <SDL2/SDL.h>

#define BOB_NB_COLS                   32
#define BOB_NB_ROWS                   6
#define BOB_FILENAME                  "assets/character.png"
#define BOB_ANIMATION_DELAY           31 // 1/32th of seconds

#define BOB_MOVE_PIXEL_PER_SECOND     100
#define BOB_JUMP_PIXEL_PER_SECOND     -400 // Varies over time, negative goes upward.
#define BOB_JUMP_CHANGE_SPEED         150 // number of ticks before changing speed
#define BOB_JUMP_THRESHOLD_SPEED      -50 // speed to trigger falling down
#define BOB_DROP_BASE_SPEED           50 // initial dropping speed

#define BOB_JUMP_PREPARE_DELAY        ( BOB_ANIMATION_DELAY * 20 )
#define BOB_JUMP_LAND_DELAY           ( BOB_ANIMATION_DELAY * 12 )
#define BOB_ANIMATION_LAND_FRAME      20

//Precalculate ratio according to a 128x128 sprite size
#define BOB_BOX_WIDTH_RATIO           ( 112.0f / 32.0f )
#define BOB_SPRITE_HEIGHT_RATIO       ( 128.0f / 112.0f )

#define BOB_ROW_MOVE_RIGHT  0
#define BOB_ROW_MOVE_LEFT   1
#define BOB_ROW_STAND       2
#define BOB_ROW_JUMP_STAND  3
#define BOB_ROW_JUMP_RIGHT  4
#define BOB_ROW_JUMP_LEFT   5

typedef enum
{
   NO_MOVE, // used for parameter passing
   STAND,
   LEFT,
   RIGHT
}
eMoveDirection;

typedef enum
{
   NO_JUMP, // used for parameter passing
   GROUND,  // currently on ground
   PREPARE, // animate and lock controls, asume on ground
   IN_AIR,  // currently in air
   LANDING  // animate and lock controls, assume on ground
}
eJumpStatus;

typedef enum
{
   SIDE_UP,
   SIDE_DOWN,
   SIDE_LEFT,
   SIDE_RIGHT
}
eCollisionSide;

typedef struct
{
   struct AnimatedSpritesheet *sheet;
   SDL_Rect                    bounding_box;
   eMoveDirection              move_direction;
   int                         y_speed;
   int                         time_at_same_speed; //counter to determine when to change speed
   eJumpStatus                 jump_status;
   int                         jump_delay; //delay for jumping and landing
   unsigned int                move_last_update;
   unsigned int                jump_last_update;
   SDL_Rect                    sprite_box; //XY offset of the sprite + final rendering size
   bool                        debug_rect; // enabled debugging rectangle around bob
}
sBobSprite;

/*
* NOTE: Bob has a bounding box for collision detection that is smaller than the
* size the sprite, while donut use the size of the sprite as a bounding box.
*
* The real bob position is calculating by offsetting the bounding box with a constant
* it is only used during rendering, else colision use the bounding box.
*
* The offset will now be recalculated according to the desired output size of
* bob on the screen. So a ratio will be used to calculate pixel offset since we
* do not know the exac pixel offset since the size of the sprite changes.
*/

/**
 * create a sprite of bob.
 *
 * @param res     Pointer on the list of resources
 * @param height  Height of the bounding box, the sprite is adjusted
 *                The width is calculated with ratios
 * @return A pointer on a bob sprite
 */
sBobSprite *bob_create ( const sResources *res, int height );

/**
 * Render bob sprite
 *
 * @param bob    The bob to render
 */
void bob_render ( sBobSprite *bob, struct Map *map );

/**
 * delete bob sprite
 *
 * @param bob    The sprite to delete
 */
void bob_delete ( sBobSprite *bob );

/**
 * Set the position of bob, used for initial positioning.
 *
 * @param bob    The sprite to position
 * @param x      the x position of the sprite
 * @param y      the y position of the sprite
 */
void bob_set_position ( sBobSprite *bob, int x, int y );


/**
 * Make bob read SDL_Event to change bob's movement according to the input
 * of the user. Also handle which key is valid to be pressed according to
 * the state of Bob.
 *
 * @param bob   The sprite to modify
 * @param event the input event to analyse
 */
void bob_give_input ( sBobSprite *bob, SDL_Event *event );

#endif // BOB_H_INCLUDED
