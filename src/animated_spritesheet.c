#include "animated_spritesheet.h"
#include "spritesheet.h"

//-------------------------
// Private Methods
//-------------------------

/**
 * Update the animation during rendering. Called by all rendering methods
 *
 * @param animatedSpritesheet    Sprite to update
 * @return the id number of the current frame to render
 */

int update_animation (struct AnimatedSpritesheet *animatedSpritesheet)
{
    int elapsed;
    if (animatedSpritesheet->running) {
        elapsed = SDL_GetTicks() - animatedSpritesheet->lastUpdate;
    } else {
        elapsed = 0;
    }
    if (elapsed > animatedSpritesheet->delayBetweenFrame) {
        int f = elapsed / animatedSpritesheet->delayBetweenFrame;
        animatedSpritesheet->currentColumn =
            (animatedSpritesheet->currentColumn + f) %
             animatedSpritesheet->spritesheet->numColumns;
        animatedSpritesheet->lastUpdate += elapsed;
    }
    return ( animatedSpritesheet->currentRow *
                       animatedSpritesheet->spritesheet->numColumns +
                       animatedSpritesheet->currentColumn );
}
//---------------------------
// Public Methods
//---------------------------

struct AnimatedSpritesheet *AnimatedSpritesheet_create( struct Spritesheet *sheet,
                                                        int delayBetweenFrame) {
    struct AnimatedSpritesheet *as;
    as = (struct AnimatedSpritesheet*)malloc(sizeof(struct AnimatedSpritesheet));
    as->spritesheet = sheet;
    as->currentRow = 0;
    as->currentColumn = 0;
    as->delayBetweenFrame = delayBetweenFrame;
    as->lastUpdate = -1;
    as->running = false;
    return as;
}

void AnimatedSpritesheet_delete(struct AnimatedSpritesheet *animatedSpritesheet) {
    if (animatedSpritesheet != NULL) {
        free(animatedSpritesheet);
    }
}

void AnimatedSpritesheet_setRow(struct AnimatedSpritesheet *animatedSpritesheet,
                                int rowNumber) {
    animatedSpritesheet->currentRow = rowNumber;
    animatedSpritesheet->currentColumn = 0;
    animatedSpritesheet->lastUpdate = SDL_GetTicks();
}

void AnimatedSpritesheet_setRow_only(struct AnimatedSpritesheet *animatedSpritesheet,
                                int rowNumber) {
    animatedSpritesheet->currentRow = rowNumber;
    animatedSpritesheet->lastUpdate = SDL_GetTicks();
}


void AnimatedSpritesheet_run(struct AnimatedSpritesheet *animatedSpritesheet) {
    animatedSpritesheet->running = true;
    animatedSpritesheet->lastUpdate = SDL_GetTicks();
}

void AnimatedSpritesheet_run_at(struct AnimatedSpritesheet *animatedSpritesheet, int column)
{
   animatedSpritesheet->running = true;
   animatedSpritesheet->currentColumn = column;
   animatedSpritesheet->lastUpdate = SDL_GetTicks();
}

void AnimatedSpritesheet_stop(struct AnimatedSpritesheet *animatedSpritesheet) {
    animatedSpritesheet->running = false;
}

void AnimatedSpritesheet_render(struct AnimatedSpritesheet *animatedSpritesheet,
                                int x, int y) {
    int currentFrame = update_animation( animatedSpritesheet);
    Spritesheet_render(animatedSpritesheet->spritesheet,
        x, y, currentFrame );
}

void AnimatedSpritesheet_render_size(struct AnimatedSpritesheet *animatedSpritesheet,
                                int x, int y, int w, int h )
{
   int currentFrame = update_animation( animatedSpritesheet );
   Spritesheet_render_size( animatedSpritesheet->spritesheet, x, y, currentFrame, w, h );
}


