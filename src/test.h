/**
 * Module that hold all the test methods to prevent recursive include issues
 */

#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED



/**
 * Test method used during development kept for reference. Test the creation
 * and rendering of bitmap fonts.
 *
 * @param app Application structure
 * @param res Resource structure
 */

void test_bitmapfont ( const sApplication *app, const sResources *res );

/**
 * Test method used during development for testing bod animation and movement.
 *
 * @param app Application structure
 * @param res Resource structure
 */
void test_bob_movement ( const sApplication *app, const sResources *res );

/**
 * test background drawing
 *
 * @param app Application structure
 * @param res Resource structure
 */
void test_draw_background ( const sApplication *app, const sResources *res );

/**
 * test scoring track
 *
 * @param app Application structure
 * @param res Resource structure
 */
void test_score_track ( const sApplication *app, const sResources *res );


#endif // TEST_H_INCLUDED
